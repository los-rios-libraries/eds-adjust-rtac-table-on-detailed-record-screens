# README #

## NO LONGER UPDATED ##
This has been incorporated into the EDS-Complete repository and any further updates will be done there.

* * *

## What this does ##

We are [displaying the RTAC table above the citation fields](https://bitbucket.org/los-rios-libraries/eds-move-rtac-table). This means that when there are a lot of holdings, the title is pushed down the page excessively. So this script

* Checks how many rows the table has
* If there are only 3 or 4 (numbers are changing as we live with it), does nothing
* If there are somewhat more, it shows the ones local to the college and hides the rest (to be revealed by "show more");
* If there are a whole lot, it only shows the first ~4, with the "show more" link available;
* adjust the display of "show more" to correct the number:
    * if the ones displayed are all of the holdings, it hides the link;
    * if there are still more to display, it replaces the number (which will now be inaccurate) with a plus sign.

## Issues ##

Ideally when there are a lot of holdings, the ones displayed would be from the local college. I haven't been able to figure out how to make that happen.