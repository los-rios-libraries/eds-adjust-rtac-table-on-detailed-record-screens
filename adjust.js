var isCatRec = window.parent.document.querySelector('.record-has-rtac');
var college = window.parent.document.getElementById('collegeID').innerHTML;

function removeParens()
{ // changes make this number misleading, so take it away. also need to figure out how to hide it when the number is the same...
  var showMore = window.parent.document.querySelector('.rtac-show-all');
  var showMoreNumber = showMore.textContent.match(/\d{1,2}/);
  var thisLocation = window.parent.document.querySelectorAll('.sel-rtac-first');
  //  console.log(Number(showMoreNumber));
  //  console.log(thisLocation.length - 1);
  if (Number(showMoreNumber) === (thisLocation.length - 1))
  {
    showMore.style.display = 'none';
  }
  else
  {
    showMore.textContent = 'More Locations';
  }
}
(function ()
{
  if (isCatRec) // only do this is rtac table is found
  {
    var collegeABR = college.toUpperCase();
    var rtacCall = setInterval(function ()
    {
      if (window.parent.document.querySelector('.rtac-table') === null) // wait for table to show up, keep trying until it does
      {
        //      console.log('rtac not found');
      }
      else
      {
        //      console.log('rtac found');
        clearInterval(rtacCall);
        var rtacRows = window.parent.document.querySelectorAll('.rtac-table tbody tr');
        //           console.log(rtacRows.length);
        if (rtacRows.length > 10) // if more than 8 holdings, simply show the first 4, regardless of location. This is not ideal...
        {
          for (var j = 0; j < rtacRows.length; j++)
          {
            if (rtacRows[j].innerHTML.indexOf('Show More') > -1)
            {
              rtacRows[j].style.display = '';
            }
            if (rtacRows[j].innerHTML.indexOf('Show Less') > -1)
            {
              rtacRows[j].style.display = 'none';
            }
            for (var k = 0; k < 4; k++) // show first 4 holdings; would be better to count the number of local ones and display those...
            {
              rtacRows[k].setAttribute('class', 'sel-rtac-first');
            }
            for (var l = 4; l < (rtacRows.length - 2); l++) // this applies from the 4th holding to the last. The final two rows are the Show More and Show Less rows
            {
              rtacRows[l].style.display = 'none';
            }
          }
          removeParens();
        }
        else if (rtacRows.length > 5) // so this is if there are enough to make it awkward, in those cases just show the local items if they exist
        {
          if (window.parent.document.querySelector('.rtac').innerHTML.indexOf(collegeABR) > -1)
          { // only do this if the local college is found. Need to redo this...
            for (var i = 0; i < rtacRows.length; i++)
            {
              if (rtacRows[i].innerHTML.indexOf(collegeABR) > -1)
              {
                rtacRows[i].setAttribute('class', 'sel-rtac-first');
                rtacRows[i].style.display = '';
              }
              else if (rtacRows[i].innerHTML.indexOf('Show More') > -1)
              {
                rtacRows[i].style.display = '';
              }
              else
              {
                rtacRows[i].setAttribute('class', '');
                rtacRows[i].style.display = 'none';
                //              rtacRows[i].removeAttribute('class');
              }
            }
          }
          else
          {
            for (var m = 0; m < 4; m++) // show first 4 holdings; would be better to count the number of local ones and display those...
            {
              rtacRows[m].setAttribute('class', 'sel-rtac-first');
            }
            for (var n = 4; n < rtacRows.length; n++) // this applies from the 4th holding to the last. The final two rows are the Show More and Show Less rows
            if (rtacRows[n].innerHTML.indexOf('Show More') > -1)
            {
              rtacRows[n].style.display = '';
            }
            else
            {
              rtacRows[n].style.display = 'none';
            }
          }
          removeParens();
        }
      }
    }, 200);
  }
})();